module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '.*\\.(css|scss)$': 'test/styleMock.ts',
    '(Title|Wrapper.js)$.*': '</test/componentMock.tsx',
  },
  testPathIgnorePatterns: ['node_modules/'],
  setupFiles: ['./test/setupEnzymeAdapter.tsx'],
  globals: {
    __APP__: true,
  },
};
