import React from 'react';

const Form = (props: any) => {
  const handleKeyDown = (e: any) => {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
    }
  };

  return (
    <form
      onSubmit={props.handleSubmit}
      onKeyDown={e => {
        handleKeyDown(e);
      }}>
      {props.children}
    </form>
  );
};

export default Form;
