"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var Form = function (props) {
    var handleKeyDown = function (e) {
        if (e.key === 'Enter' && e.shiftKey === false) {
            e.preventDefault();
        }
    };
    return (react_1.default.createElement("form", { onSubmit: props.handleSubmit, onKeyDown: function (e) {
            handleKeyDown(e);
        } }, props.children));
};
exports.default = Form;
//# sourceMappingURL=Form.js.map