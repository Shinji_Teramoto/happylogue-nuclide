export interface IFullScreenDialogReducer {
    fullScreenOpen: boolean;
    fullScreenName: string;
}
export declare const initialState: IFullScreenDialogReducer;
declare const reducer: (state: IFullScreenDialogReducer | undefined, action: any) => {
    fullScreenOpen: boolean;
    fullScreenName: any;
};
export default reducer;
