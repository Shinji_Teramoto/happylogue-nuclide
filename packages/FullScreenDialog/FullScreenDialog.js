"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_redux_1 = require("react-redux");
var recompose_1 = require("recompose");
var styles_1 = require("@material-ui/core/styles");
var Dialog_1 = __importDefault(require("@material-ui/core/Dialog"));
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var Close_1 = __importDefault(require("@material-ui/icons/Close"));
var Slide_1 = __importDefault(require("@material-ui/core/Slide"));
var action_1 = require("./action");
function transition(props) {
    return react_1.default.createElement(Slide_1.default, __assign({ direction: "up" }, props));
}
var FullScreenDialog = function (_a) {
    var children = _a.children, classes = _a.classes, fullScreenOpen = _a.fullScreenOpen, handleCloseFullScreenDialog = _a.handleCloseFullScreenDialog;
    return (react_1.default.createElement(Dialog_1.default, { fullScreen: true, open: fullScreenOpen, disableBackdropClick: true, onClose: handleCloseFullScreenDialog, TransitionComponent: transition },
        react_1.default.createElement(IconButton_1.default, { className: classes.closeButton, color: "inherit", onClick: handleCloseFullScreenDialog, "aria-label": "Close" },
            react_1.default.createElement(Close_1.default, null)),
        children));
};
var styles = function (theme) { return ({
    closeButton: {
        position: 'fixed',
        top: theme.spacing.unit * 1.4,
        left: theme.spacing.unit * 1.4,
        zIndex: 1400,
        color: '#757575',
        fontSize: 'bold',
    },
}); };
function mapStateToProps(state) {
    return {
        fullScreenOpen: state.fullScreenDialog.fullScreenOpen,
    };
}
var mapDispatchToProps = {
    handleCloseFullScreenDialog: action_1.actions.handleCloseFullScreenDialog,
};
exports.default = recompose_1.compose(styles_1.withStyles(styles, { name: 'FullScreenDialog', withTheme: true }), react_redux_1.connect(mapStateToProps, mapDispatchToProps))(FullScreenDialog);
//# sourceMappingURL=FullScreenDialog.js.map