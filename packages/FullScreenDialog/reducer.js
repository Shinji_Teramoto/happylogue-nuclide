"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
exports.initialState = {
    fullScreenOpen: false,
    fullScreenName: '',
};
var reducer = function (state, action) {
    if (state === void 0) { state = exports.initialState; }
    switch (action.type) {
        case constants_1.ActionTypes.OPEN_FULLSCREEN_DIALOG: {
            return __assign({}, state, { fullScreenOpen: true, fullScreenName: action.payload });
        }
        case constants_1.ActionTypes.CLOSE_FULLSCREEN_DIALOG: {
            return __assign({}, state, { fullScreenOpen: false });
        }
        default: {
            return state;
        }
    }
};
exports.default = reducer;
//# sourceMappingURL=reducer.js.map