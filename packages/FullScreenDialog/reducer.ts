import {ActionTypes} from './constants';
import {IHandleFullScreenDialogAction} from './action';

export interface IFullScreenDialogReducer {
  fullScreenOpen: boolean;
  fullScreenName: string;
}

export const initialState: IFullScreenDialogReducer = {
  fullScreenOpen: false,
  fullScreenName: '',
};
const reducer = (state: IFullScreenDialogReducer = initialState, action: IHandleFullScreenDialogAction) => {
  switch (action.type) {
    case ActionTypes.OPEN_FULLSCREEN_DIALOG: {
      return {
        ...state,
        fullScreenOpen: true,
        fullScreenName: action.payload,
      };
    }

    case ActionTypes.CLOSE_FULLSCREEN_DIALOG: {
      return {
        ...state,
        fullScreenOpen: false,
      };
    }

    default: {
      return state;
    }
  }
};
export default reducer;
