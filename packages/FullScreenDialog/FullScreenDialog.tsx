import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'recompose';
import {withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {actions} from './action';

interface Props {
  classes: any
}

interface DispatchProps {
  fullScreenOpen: boolean;
  handleCloseFullScreenDialog(): void
}

function transition(props: any) {
  return <Slide direction="up" {...props} />;
}

const FullScreenDialog:React.SFC<Props & DispatchProps> = ({children, classes, fullScreenOpen, handleCloseFullScreenDialog}) => (
  <Dialog
    fullScreen={true}
    open={fullScreenOpen}
    disableBackdropClick={true}
    onClose={handleCloseFullScreenDialog}
    TransitionComponent={transition}>
    <IconButton
      className={classes.closeButton}
      color="inherit"
      onClick={handleCloseFullScreenDialog}
      aria-label="Close">
      <CloseIcon />
    </IconButton>
    {children}
  </Dialog>
);


const styles: any = (theme: any) => ({
  closeButton: {
    position: 'fixed',
    top: theme.spacing.unit * 1.4,
    left: theme.spacing.unit * 1.4,
    zIndex: 1400,
    color: '#757575',
    fontSize: 'bold',
  },
});

function mapStateToProps(state: any) {
  return {
    fullScreenOpen: state.fullScreenDialog.fullScreenOpen,
  };
}

const mapDispatchToProps = {
  handleCloseFullScreenDialog: actions.handleCloseFullScreenDialog,
};

export default compose<DispatchProps & Props, {}>(
  withStyles(styles, {name: 'FullScreenDialog', withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps)
)(FullScreenDialog);