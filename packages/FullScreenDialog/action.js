"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typesafe_actions_1 = require("typesafe-actions");
var constants_1 = require("./constants");
exports.actions = {
    handleCloseFullScreenDialog: typesafe_actions_1.createAction(constants_1.ActionTypes.CLOSE_FULLSCREEN_DIALOG),
    handleOpenFullScreenDialog: typesafe_actions_1.createAction(constants_1.ActionTypes.OPEN_FULLSCREEN_DIALOG, function (resolve) {
        return function (name) {
            if (name === void 0) { name = ''; }
            return resolve(name);
        };
    })
};
//# sourceMappingURL=action.js.map