import { ActionType } from 'typesafe-actions';
import { ActionTypes } from './constants';
export declare const actions: {
    handleCloseFullScreenDialog: () => {
        type: ActionTypes.CLOSE_FULLSCREEN_DIALOG;
    };
    handleOpenFullScreenDialog: any;
};
export declare type IHandleFullScreenDialogAction = ActionType<typeof actions>;
