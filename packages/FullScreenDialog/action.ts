import {ActionType, createAction} from 'typesafe-actions';
import {ActionTypes} from './constants';

export const actions = {
  handleCloseFullScreenDialog: createAction(ActionTypes.CLOSE_FULLSCREEN_DIALOG),
  handleOpenFullScreenDialog:  createAction(ActionTypes.OPEN_FULLSCREEN_DIALOG, (resolve): any => {
    return (name: string = '') => resolve(name);
  })
};

export type IHandleFullScreenDialogAction = ActionType<typeof actions>;
