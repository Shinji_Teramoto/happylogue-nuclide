export declare enum ActionTypes {
    CLOSE_FULLSCREEN_DIALOG = "CLOSE_FULLSCREEN_DIALOG",
    OPEN_FULLSCREEN_DIALOG = "OPEN_FULLSCREEN_DIALOG"
}
