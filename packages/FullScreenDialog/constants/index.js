"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionTypes;
(function (ActionTypes) {
    ActionTypes["CLOSE_FULLSCREEN_DIALOG"] = "CLOSE_FULLSCREEN_DIALOG";
    ActionTypes["OPEN_FULLSCREEN_DIALOG"] = "OPEN_FULLSCREEN_DIALOG";
})(ActionTypes = exports.ActionTypes || (exports.ActionTypes = {}));
//# sourceMappingURL=index.js.map