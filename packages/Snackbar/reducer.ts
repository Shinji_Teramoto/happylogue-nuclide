'use strict';
import {ActionTypes} from './constants';
import {ISnackbarAction} from './action';

export interface ISnackbarReducer {
  open: boolean;
  message: string;
}

export interface IStore {
  snackbar: ISnackbarReducer;
}

export const initialState: ISnackbarReducer = {
  open: false,
  message: '',
};
const reducer = (state: ISnackbarReducer = initialState, action: ISnackbarAction) => {
  switch (action.type) {
    case ActionTypes.OPEN_MESSAGETE_SNACBAR: {
      return {
        ...state,
        open: true,
        message: action.payload.message,
      };
    }

    case ActionTypes.CLOSE_MESSAGETE_SNACBAR: {
      return {
        ...state,
        open: false,
      };
    }

    default: {
      return state;
    }
  }
};
export default reducer;
