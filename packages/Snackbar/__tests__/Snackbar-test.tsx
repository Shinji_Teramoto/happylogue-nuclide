import React from 'react';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store';
import Snackbar from '../Snackbar';

const middlewares: any = [];
const mockStore = configureStore(middlewares);

describe('Snackbar', function() {
  test('should render Registration', () => {
    const initialState = {snackbar: {message: 'Your registration is complete.', open: true}};
    const store = mockStore(initialState);
    const props = {store};
    const wrapper = mount(<Snackbar {...props} vertical='top'/>);
    expect(wrapper.find('[data-test="message"]').text()).toBe('Your registration is complete.');
  });
});
