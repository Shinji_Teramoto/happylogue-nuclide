import {ActionTypes} from '../constants';
import reducer from '../reducer';

describe('reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      open: false,
      message: '',
    });
  });

  it('should handle OPEN_MESSAGETE', () => {
    expect(
      reducer([], {
        type: ActionTypes.OPEN_MESSAGETE_SNACBAR,
        payload: {
          open: true,
          message: 'success',
        },
      }),
    ).toEqual({
      open: true,
      message: 'success',
    });
  });

  it('should handle CLOSE_MESSAGETE', () => {
    expect(
      reducer([], {
        type: ActionTypes.CLOSE_MESSAGETE_SNACBAR,
        open: false,
      }),
    ).toEqual({
      open: false,
    });
  });
});
