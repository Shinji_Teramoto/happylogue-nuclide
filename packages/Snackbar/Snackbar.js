"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var react_redux_1 = require("react-redux");
var recompose_1 = require("recompose");
var Snackbar_1 = __importDefault(require("@material-ui/core/Snackbar"));
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var Close_1 = __importDefault(require("@material-ui/icons/Close"));
var action_1 = require("./action");
var Snackbar = function (_a) {
    var vertical = _a.vertical, snackbar = _a.snackbar, handleMessageCloseAction = _a.handleMessageCloseAction;
    var message = snackbar.message, open = snackbar.open;
    return (react_1.default.createElement(react_1.Fragment, null,
        react_1.default.createElement(Snackbar_1.default, { anchorOrigin: {
                vertical: vertical,
                horizontal: 'center',
            }, open: open, onClose: handleMessageCloseAction, ContentProps: {
                'aria-describedby': 'message-id',
            }, message: react_1.default.createElement("span", { id: "message-id", "data-test": "message" }, message), action: [
                react_1.default.createElement(IconButton_1.default, { key: "close", "aria-label": "Close", color: "inherit", onClick: handleMessageCloseAction },
                    react_1.default.createElement(Close_1.default, null)),
            ] })));
};
function mapStateToProps(state) {
    return {
        snackbar: state.snackbar,
    };
}
var mapDispatchToProps = {
    handleMessageCloseAction: action_1.actions.handleMessageCloseAction,
};
exports.default = recompose_1.compose(react_redux_1.connect(mapStateToProps, mapDispatchToProps))(Snackbar);
//# sourceMappingURL=Snackbar.js.map