export interface ISnackbarReducer {
    open: boolean;
    message: string;
}
export interface IStore {
    snackbar: ISnackbarReducer;
}
export declare const initialState: ISnackbarReducer;
declare const reducer: (state: ISnackbarReducer | undefined, action: any) => {
    open: boolean;
    message: any;
};
export default reducer;
