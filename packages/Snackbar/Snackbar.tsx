import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {compose} from 'recompose';
import Snackbars from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {actions} from './action';
import {ISnackbarReducer, IStore} from './reducer';

interface DispatchProps {
  snackbar: ISnackbarReducer;
  handleMessageCloseAction(): void
}

interface StateProps {
  vertical: 'top' | 'bottom';
}

const Snackbar:React.SFC<DispatchProps & StateProps> = ({vertical, snackbar, handleMessageCloseAction}) => {
  const {message, open} = snackbar;
  return (
    <Fragment>
      <Snackbars
        anchorOrigin={{
          vertical: vertical,
          horizontal: 'center',
        }}
        open={open}
        onClose={handleMessageCloseAction}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id" data-test="message">{message}</span>}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={handleMessageCloseAction}>
            <CloseIcon />
          </IconButton>,
        ]}
      />
    </Fragment>
  );
}

function mapStateToProps(state: IStore) {
  return {
    snackbar: state.snackbar,
  };
}

const mapDispatchToProps = {
  handleMessageCloseAction: actions.handleMessageCloseAction,
};

export default compose<DispatchProps & StateProps, StateProps>(
  connect(mapStateToProps, mapDispatchToProps)
)(Snackbar);
