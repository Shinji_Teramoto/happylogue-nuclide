import { Dispatch } from 'redux';
import { ActionType } from 'typesafe-actions';
import { ActionTypes } from './constants';
export declare const actions: {
    handleMessageCloseAction: () => {
        type: ActionTypes.CLOSE_MESSAGETE_SNACBAR;
    };
    handleMessageOpenAction: any;
};
export declare type ISnackbarAction = ActionType<typeof actions>;
export declare function handleMessageOpen(message: string, delay?: number): (dispatch: Dispatch<any>) => void;
