import {Dispatch} from 'redux';
import {ActionType, createAction} from 'typesafe-actions';
import {ActionTypes} from './constants';
import {ISnackbarReducer} from './reducer';

export const actions = {
  handleMessageCloseAction: createAction(ActionTypes.CLOSE_MESSAGETE_SNACBAR),
  handleMessageOpenAction:  createAction(ActionTypes.OPEN_MESSAGETE_SNACBAR, (resolve): any => {
    return (message: ISnackbarReducer) => resolve(message);
  })
}

export type ISnackbarAction = ActionType<typeof actions>;

export function handleMessageOpen(message: string, delay = 0) {
  return (dispatch: Dispatch<any>) => {
    dispatch(actions.handleMessageOpenAction({message: message}));
    if(delay > 0) {
      setTimeout(() => {
        dispatch(actions.handleMessageCloseAction());
      }, delay);
    }
  };
}
