import React from 'react';
interface StateProps {
    vertical: 'top' | 'bottom';
}
declare const _default: React.ComponentClass<StateProps, any>;
export default _default;
