"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var typesafe_actions_1 = require("typesafe-actions");
var constants_1 = require("./constants");
exports.actions = {
    handleMessageCloseAction: typesafe_actions_1.createAction(constants_1.ActionTypes.CLOSE_MESSAGETE_SNACBAR),
    handleMessageOpenAction: typesafe_actions_1.createAction(constants_1.ActionTypes.OPEN_MESSAGETE_SNACBAR, function (resolve) {
        return function (message) { return resolve(message); };
    })
};
function handleMessageOpen(message, delay) {
    if (delay === void 0) { delay = 0; }
    return function (dispatch) {
        dispatch(exports.actions.handleMessageOpenAction({ message: message }));
        if (delay > 0) {
            setTimeout(function () {
                dispatch(exports.actions.handleMessageCloseAction());
            }, delay);
        }
    };
}
exports.handleMessageOpen = handleMessageOpen;
//# sourceMappingURL=action.js.map